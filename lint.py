"""lint the package"""
import subprocess

PKG_NAME = "checksystem"

for command in [
        "pycodestyle " + PKG_NAME,
        "pyflakes " + PKG_NAME,
        "pylint " + PKG_NAME,
]:
    print("Command:", command)
    subprocess.call(command)
