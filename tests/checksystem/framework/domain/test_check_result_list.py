"""Test CheckResultList"""
from unittest.mock import MagicMock
from checksystem.framework.domain.check_result_list import CheckResultList


class TestCheckResultList:
    """Test CheckResultList"""
    def test_length_increases_1_when_added(self):
        """Test add"""
        # given initialize
        check_result_list = CheckResultList()
        assert not check_result_list.get_list()

        # when added
        check_result = MagicMock()
        check_result_list.add(check_result)

        # then length is 1
        assert len(check_result_list.get_list()) == 1
