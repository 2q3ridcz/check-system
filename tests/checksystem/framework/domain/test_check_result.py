"""Test CheckResult"""
from checksystem.framework.domain.check_result import CheckResult


class TestCheckResult:
    """Test CheckResult"""
    def test_holds_values_when_initialized(self):
        """test constructor"""
        # given none
        # when initialized
        check_result = CheckResult(
            check_name="some check",
            succeeded=True,
            description="OK: 90, NG: 0, Skip: 10",
            link="./SomeCheck/index.html",
        )

        # then holds values
        assert check_result.check_name == "some check"
        assert check_result.succeeded
        assert check_result.description == "OK: 90, NG: 0, Skip: 10"
        assert check_result.link == "./SomeCheck/index.html"
