"""CheckLogic をテストする"""
import pytest
from checksystem.framework.domain.check_logic import CheckLogic


class TestCheckLogic:
    """CheckLogic をテストする"""
    def test_raises_when_initialized(self):
        """インスタンス化すると異常発生する"""
        # given none
        with pytest.raises(TypeError):
            # when initialized
            CheckLogic()

            # then raises

    def test_raises_when_initialized_given_implemented_without_execute_method(self):
        """execute メソッドのない実装はインスタンス化すると異常発生する"""
        # given implemented without execute method
        class CheckLogicWithoutExecute(CheckLogic):
            """abstruct class with abstruct method not implemented"""

        with pytest.raises(TypeError):
            # when initialized
            CheckLogicWithoutExecute()

            # then raises

    def test_can_call_execute_method_given_implemented_with_execute_method(self):
        """execute メソッドのある実装の場合はインスタンス化できる"""
        # given implemented with execute method
        class CheckLogicWithExecute(CheckLogic):
            """abstruct class with abstruct method implemented"""
            def execute(self):
                """implements abstruct method"""

        # when initialized
        check_logic = CheckLogicWithExecute()

        # then can call execute
        check_logic.execute()
