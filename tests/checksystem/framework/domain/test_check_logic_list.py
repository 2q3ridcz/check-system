"""Test CheckLogicList"""
from unittest.mock import MagicMock
from checksystem.framework.domain.check_logic_list import CheckLogicList


class TestCheckLogicList:
    """Test CheckLogicList"""
    def test_length_increases_1_when_added(self):
        """Test add"""
        # given initialize
        check_logic_list = CheckLogicList()
        assert not check_logic_list.get_list()

        # when added
        check_logic = MagicMock()
        check_logic_list.add(check_logic)

        # then length is 1
        assert len(check_logic_list.get_list()) == 1

        # when added 1 more
        check_logic_list.add(check_logic)

        # then length is 2
        assert len(check_logic_list.get_list()) == 2


class TestExecuteMethodOfCheckLogicList:
    """Test CheckLogicList.execute"""
    def test_return_0_result_when_executed_given_0_check_logic(self):
        """Test execute"""
        # given 0 CheckLogic
        check_logic_list = CheckLogicList()

        # when executed
        check_result_list = check_logic_list.execute()

        # then return 0 result
        assert not check_result_list.get_list()

    def test_return_1_result_when_executed_given_1_check_logic(self):
        """Test execute"""
        # given 1 CheckLogic
        check_logic_list = CheckLogicList()
        check_logic = MagicMock()
        check_logic_list.add(check_logic)

        # when executed
        check_result_list = check_logic_list.execute()

        # then return 1 result
        assert len(check_result_list.get_list()) == 1

    def test_return_2_result_when_executed_given_2_check_logic(self):
        """Test execute"""
        # given 2 CheckLogic
        check_logic_list = CheckLogicList()
        check_logic = MagicMock()
        check_logic_list.add(check_logic)
        check_logic_list.add(check_logic)

        # when executed
        check_result_list = check_logic_list.execute()

        # then return 2 result
        assert len(check_result_list.get_list()) == 2
