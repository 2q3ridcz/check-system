"""CheckResultListHtmlWriter をテストする"""
from unittest.mock import MagicMock
from checksystem.framework.presentation.check_result_list_html_writer import (
    CheckResultListHtmlWriter
)


class TestCheckResultListHtmlWriter:
    """CheckResultListHtmlWriter をテストする"""
    # TODO: create test
    def test_(self, tmp_path):
        """test"""
        # given
        presenter = CheckResultListHtmlWriter(out_folder=str(tmp_path.resolve()))
        assert not tmp_path.joinpath("check-result-list.html").exists()

        check_result_list = MagicMock()

        # when
        presenter.execute(check_result_list=check_result_list)

        # then
        assert tmp_path.joinpath("check-result-list.html").exists()
