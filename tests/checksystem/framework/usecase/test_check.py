"""Test Check"""
from unittest.mock import MagicMock, Mock
from checksystem.framework.usecase.check import Check


class TestCheck:
    """Test Check"""
    def test_result_of_check_result_list_is_passed_to_presenter(self):
        """test constructor"""
        # given check_result_list and presenter
        check_logic_list = MagicMock()
        check_logic_list.execute = Mock(
            return_value="values_to_pass"
        )

        check_result_list_presenter = MagicMock()
        check_result_list_presenter.execute = Mock(
            side_effect=lambda check_result_list: check_result_list
        )

        # when executed
        check = Check(
            check_logic_list=check_logic_list,
            check_result_list_presenter=check_result_list_presenter,
        )
        result = check.execute()

        # then result of check_result_list is passed to presenter
        assert result == "values_to_pass"
