"""CheckResultListPresenter をテストする"""
import pytest
from checksystem.framework.usecase.check_result_list_presenter import CheckResultListPresenter


class TestCheckResultListPresenter:
    """CheckResultListPresenter をテストする"""
    def test_raises_when_initialized(self):
        """インスタンス化すると異常発生する"""
        # given none
        with pytest.raises(TypeError):
            # when initialized
            CheckResultListPresenter()

            # then raises

    def test_raises_when_initialized_given_implemented_without_execute_method(self):
        """execute メソッドのない実装はインスタンス化すると異常発生する"""
        # given implemented without execute method
        class CheckResultListPresenterWithoutExecute(CheckResultListPresenter):
            """abstruct class with abstruct method not implemented"""

        with pytest.raises(TypeError):
            # when initialized
            CheckResultListPresenterWithoutExecute()

            # then raises

    def test_can_call_execute_method_given_implemented_with_execute_method(self):
        """execute メソッドのある実装の場合はインスタンス化できる

        ## メモ

        抽象メソッドの実装は強制できるが、抽象メソッドの引数の実装は
        強制できない模様。
        """
        # given implemented with execute method
        class CheckResultListPresenterWithExecute(CheckResultListPresenter):
            """abstruct class with abstruct method implemented"""
            def execute(self):
                """implements abstruct method"""

        # when initialized
        check_logic = CheckResultListPresenterWithExecute()

        # then can call execute
        check_logic.execute()
