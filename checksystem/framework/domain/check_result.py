"""CheckLogic の結果を保持する"""
from dataclasses import dataclass


@dataclass(init=False, eq=True, frozen=True)
class CheckResult:
    """CheckLogic の結果を保持する

    TODO: パラメータの型をチェックするかを決める
    """
    check_name: str
    succeeded: bool
    description: str
    link: str

    def __init__(
            self,
            check_name,
            succeeded,
            description,
            link,
    ):
        object.__setattr__(self, "check_name", check_name)
        object.__setattr__(self, "succeeded", succeeded)
        object.__setattr__(self, "description", description)
        object.__setattr__(self, "link", link)
