"""複数の CheckLogic を保持する"""
import typing

from .check_logic import CheckLogic
from .check_result_list import CheckResultList


class CheckLogicList:
    """複数の CheckLogic を保持する"""

    def __init__(self):
        self.values: typing.List[CheckLogic] = []

    def add(self, check_logic: CheckLogic):
        """CheckLogic を追加する"""
        self.values.append(check_logic)

    def get_list(self) -> typing.List[CheckLogic]:
        """CheckLogic のリストを応答する"""
        return self.values

    def execute(self) -> CheckResultList:
        """チェックを実行する"""
        check_result_list = CheckResultList()
        for check_logic in self.get_list():
            # TODO: check_logic.execute() のエラーハンドリングを追加
            check_result_list.add(check_logic.execute())
        return check_result_list
