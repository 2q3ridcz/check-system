"""CheckLogic インターフェース

チェック処理を作成する場合はこのインターフェースを実装(抽象クラスを継承)すること。
実装クラス作成時は下記事項を守ること。
- 引数なしで呼び出す executeメソッドがチェックを実行すること。
- executeメソッドはチェック結果を CheckResultオブジェクトで返却すること。
- チェック結果の詳細を保存したい場合は報告ファイルを作り、
    CheckResultオブジェクトの Linkパラメータにパスを渡すこと。
    これにより CheckResultListHtmlWriterオブジェクトがチェック結果一覧htmlから
    そのパスへのリンクを表示してくれる。
- 実装後は実装クラスを main (console_checkall.py) に追加すること。
    これによりチェック実行対象となる。
"""
from abc import ABC, abstractmethod
from .check_result import CheckResult


class CheckLogic(ABC):
    """CheckLogic インターフェース

    チェック処理を作成する場合はこのインターフェースを実装(抽象クラスを継承)すること。
    実装クラス作成時は下記事項を守ること。
    - 引数なしで呼び出す executeメソッドがチェックを実行すること。
    - executeメソッドはチェック結果を CheckResultオブジェクトで返却すること。
    - チェック結果の詳細を保存したい場合は報告ファイルを作り、
        CheckResultオブジェクトの Linkパラメータにパスを渡すこと。
        これにより CheckResultListHtmlWriterオブジェクトがチェック結果一覧htmlから
        そのパスへのリンクを表示してくれる。
    - 実装後は実装クラスを main (console_checkall.py) に追加すること。
        これによりチェック実行対象となる。
    """
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def execute(self) -> CheckResult:
        """チェックを実行する"""
        raise NotImplementedError
