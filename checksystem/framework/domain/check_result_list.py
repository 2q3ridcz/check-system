"""複数の CheckResult を保持する"""
import typing

from .check_result import CheckResult


class CheckResultList:
    """複数の CheckResult を保持する"""

    def __init__(self):
        self.values: typing.List[CheckResult] = []

    def add(self, check_result):
        """CheckResult を追加する"""
        self.values.append(check_result)

    def get_list(self):
        """CheckResult のリストを応答する"""
        return self.values
