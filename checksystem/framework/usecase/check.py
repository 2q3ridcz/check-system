"""CheckLogic を実行する"""
from ..domain.check_logic_list import CheckLogicList
from .check_result_list_presenter import CheckResultListPresenter


class Check:
    """CheckLogic を実行する"""
    # pylint: disable=too-few-public-methods
    def __init__(
            self,
            check_logic_list: CheckLogicList,
            check_result_list_presenter: CheckResultListPresenter,
    ):
        self.check_logic_list = check_logic_list
        self.check_result_list_presenter = check_result_list_presenter

    def execute(self):
        """チェックを実行する"""
        check_logic_list = self.check_logic_list
        check_result_list_presenter = self.check_result_list_presenter

        check_result_list = check_logic_list.execute()

        return check_result_list_presenter.execute(
            check_result_list=check_result_list
        )
