"""CheckResultListPresenter のインターフェース"""
from abc import ABC, abstractmethod
from ..domain.check_result_list import CheckResultList


class CheckResultListPresenter(ABC):
    """CheckResultListPresenter インターフェース"""
    # pylint: disable=too-few-public-methods
    @abstractmethod
    def execute(self, check_result_list: CheckResultList):
        """チェック結果を表示する"""
        raise NotImplementedError
