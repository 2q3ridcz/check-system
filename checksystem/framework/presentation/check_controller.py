"""CheckController"""
import typing
from ..domain.check_logic import CheckLogic
from ..domain.check_logic_list import CheckLogicList
from ..usecase.check import Check
from ..presentation.check_result_list_html_writer import CheckResultListHtmlWriter


class CheckController:
    """CheckController"""
    # pylint: disable=too-few-public-methods
    def __init__(self):
        pass

    def check_and_create_html_report(
            self,
            check_logics: typing.List[CheckLogic],
            out_folder: str,
    ):
        """Checkの入力オブジェクトを作成して Checkを実行する"""
        check_logic_list = CheckLogicList()
        for check_logic in check_logics:
            check_logic_list.add(check_logic)

        presenter = CheckResultListHtmlWriter(out_folder=out_folder)

        check = Check(
            check_logic_list=check_logic_list,
            check_result_list_presenter=presenter,
        )
        check.execute()
