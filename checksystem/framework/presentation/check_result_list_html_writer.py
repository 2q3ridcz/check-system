"""CheckResultListPresenter の実装"""
import pathlib
# import jinja2
from ..domain.check_result_list import CheckResultList
from ..usecase.check_result_list_presenter import CheckResultListPresenter


class CheckResultListHtmlWriter(CheckResultListPresenter):
    """CheckResultListPresenter の実装"""
    # pylint: disable=too-few-public-methods
    def __init__(self, out_folder: str):
        # TODO: 存在しないパスを受けた場合は raiseする
        self.out_folder = out_folder

    def execute(self, check_result_list: CheckResultList):
        """チェック結果を Htmlファイルに出力する"""
        out_folder = self.out_folder

        out_folder_path = pathlib.Path(out_folder).resolve()
        out_file_path = out_folder_path.joinpath("check-result-list.html")
        # TODO: Use jinja2
        # template_path = 'check_result_list_html_writer_jinja2.html'

        # data = check_result_list.get_list()

        # env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
        # template = env.get_template(template_path)
        # rendered = template.render(data=data)
        rendered = '''
<!doctype html>
<html lang="ja" data-color-mode="auto" data-light-theme="light" data-dark-theme="dark_dimmed">
<head>
  <meta charset="utf-8"/>
  <style type="text/css">
    table { border: 2px solid; border-collapse: collapse; }
    th { border: 2px solid; }
    td { border: 2px solid; }
  </style>
  <link href="../css/global-dark-css-theme.css" rel="stylesheet" />
  <title>チェック結果一覧</title>
</head>
<body>
  <h1>チェック結果一覧</h1>
  <table>
    <tr><th>チェック名</th><th>成否</th><th>チェック結果概要</th><th>チェック結果詳細</th>'''
        for check_result in check_result_list.get_list():
            rendered += """
    <tr>"""
            rendered += "<td>" + check_result.check_name + "</td>"
            rendered += "<td>" + str(check_result.succeeded) + "</td>"
            rendered += "<td>" + check_result.description + "</td>"
            rendered += (
                "<td>"
                "<a href='" + check_result.link + "'>"
                "" + check_result.link + "</a>"
                "</td>"
            )
            rendered += "</tr>"
        rendered += """
  </table>
</body></html>"""
        with open(out_file_path, "w", encoding="Utf-8") as file:
            file.write(rendered)
