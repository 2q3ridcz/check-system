"""CheckLogic の実装サンプル"""
import pathlib
from ..framework.domain.check_logic import CheckLogic
from ..framework.domain.check_result import CheckResult


class SampleCheckLogic(CheckLogic):
    """CheckLogic の実装サンプル"""
    # pylint: disable=too-few-public-methods
    def __init__(self, out_folder: str):
        self.out_folder = out_folder

    def execute(self) -> CheckResult:
        """チェックを実行したフリをして、テキトーな CheckResultを応答する。"""
        out_folder = self.out_folder
        check_name = self.__class__.__name__

        # 出力フォルダを作成 (この部分を frameworkの apiとしてもいいかも)
        out_folder_path = pathlib.Path(out_folder)
        check_logic_folder_path = out_folder_path.joinpath(check_name)
        check_logic_folder_path.mkdir(parents=True, exist_ok=True)

        # チェック実行
        # ここでは実行したフリをしてテキトーなチェック結果詳細を出力する
        out_file_path = check_logic_folder_path.joinpath("index.html")
        out_file_path.write_text(
            data="""<html><body>
チェックしたフリしてました…ごめんなさい…
</body></html>
""",
            encoding="utf_8",
        )

        # CheckResultオブジェクトを応答する
        # ここではテキトーな内容で。
        link = "./" + out_file_path.relative_to(out_folder_path).as_posix()
        check_result = CheckResult(
            check_name=check_name,
            succeeded=True,
            description="まぁええんちゃう？",
            link=link,
        )
        return check_result
