# check-system

ユーザーが実装するチェックロジックを実行し、その結果をチェック結果一覧として出力する。

## 実行方法

check_main.py を実行する。  
このプロジェクトのルートから実行する場合、以下のコマンドで実行可能。

```shell
python ./console_check/check_main.py
```

チェック結果一覧htmlが ./console_check/Output/src/check-result-list.html に出力される。

## チェックロジック追加方法

./checksystem/framework/domain/check_logic.py インターフェースを実装 (抽象クラス継承) をすてチェックロジックを作成する。
詳細な説明は check_logic.py のソースコードのコメント参照。

チェックロジックの実装サンプル ./checksystem/user_coding/sample_check_logic.py もご参考。

作成したチェックロジックは ./console_check/check_main.py に追加する。
こちらも check_main.py のソースコードのコメントで説明しているのでご参照。

## Thanks to

This project uses css from [Global Dark CSS Theme · GitHub](https://gist.github.com/meskarune/2721790).
