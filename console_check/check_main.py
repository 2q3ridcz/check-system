"""全てのチェックを実行し、チェック結果一覧htmlを作成する。

CheckLogicオブジェクトを組み立て、CheckControllerに渡す。

## チェック処理追加手順

1. CheckLogic の実装クラスを用意する。
2. CheckLogic の実装クラスから CheckLogicオブジェクトを作成する。
3. CheckControllerの check_and_create_html_reportメソッドの
    check_logics引数の配列に CheckLogicオブジェクトを追加する。
"""
# pylint: disable=wrong-import-position
import pathlib
import sys

SELF_PATH = pathlib.Path(__file__).resolve()
PKG_PATH = SELF_PATH.joinpath("../../").resolve()
if str(PKG_PATH) not in sys.path:
    sys.path.insert(0, str(PKG_PATH))

from checksystem.framework.presentation.check_controller import CheckController
from checksystem.user_coding.sample_check_logic import SampleCheckLogic


def check_main():
    """main"""
    out_folder = SELF_PATH.joinpath("../Output/src").resolve()

    controller = CheckController()
    controller.check_and_create_html_report(
        check_logics=[
            # ここにチェックロジックを追加していく
            SampleCheckLogic(out_folder=str(out_folder)),
            SampleCheckLogic(out_folder=str(out_folder)),
        ],
        out_folder=str(out_folder),
    )


if __name__ == '__main__':
    check_main()
